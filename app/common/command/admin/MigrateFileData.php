<?php

declare(strict_types=1);

namespace app\common\command\admin;

use base\common\command\admin\MigrateFileDataBase;

class MigrateFileData extends MigrateFileDataBase
{
    protected $fromDomain = [];

    protected $toDomain = 'https://admin.demo.ulthon.com';
}
