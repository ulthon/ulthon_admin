# 基于官方 PHP 8.0 镜像
FROM php:8.0-fpm

RUN sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list 
RUN sed -i 's/security.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list

RUN apt-get update 

# 安装nginx
RUN apt-get install -y nginx

# 安装ffmpeg
# RUN apt-get install -y ffmpeg

# 安装redis
# RUN apt-get install -y redis-server

# 安装git
# RUN apt-get install -y git

ADD --chmod=0755 https://delivery.ulthon.com/install-php-extensions /usr/local/bin/

RUN install-php-extensions pdo_mysql
RUN install-php-extensions gd
RUN install-php-extensions fileinfo
RUN install-php-extensions opcache
RUN install-php-extensions redis
RUN install-php-extensions event
RUN install-php-extensions imagick
RUN install-php-extensions zip

# 清理默认 Nginx 配置
RUN rm /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

# 设置工作目录
WORKDIR /var/www/html
# 将当前目录下的文件拷贝到工作目录
COPY . /var/www/html

# 内部安装compsoer并安装依赖，如果不需要可以注释掉
# RUN install-php-extensions @composer

# 创建runtime目录
RUN mkdir -p /var/www/html/runtime
RUN chmod -R 777 /var/www/html/runtime
VOLUME /var/www/html/runtime

# 创建storage目录
RUN mkdir -p /var/www/html/public/storage
RUN chmod -R 777 /var/www/html/public/storage
VOLUME /var/www/html/public/storage

# 创建build目录
RUN mkdir -p /var/www/html/public/build
RUN chmod -R 777 /var/www/html/public/build
VOLUME /var/www/html/public/build

# 创建storage（safe）目录
RUN mkdir -p /var/www/html/storage
RUN chmod -R 777 /var/www/html/storage
VOLUME /var/www/html/storage

# 挂载主目录，也可以选择直接挂载主目录，可以把上面的几个指定的目录删掉
# VOLUME ["/var/www/html"]

# 暴露 Nginx 端口
EXPOSE 80

# 
RUN chmod +x /var/www/html/docker/run.sh

# 启动 Nginx PHP 然后阻塞
ENTRYPOINT ["/var/www/html/docker/run.sh"]

CMD ["server"]

