<?php

// 配置参考：https://doc.ulthon.com/read/augushong/ulthon_admin/timer-mode/zh-cn/2.x.html
$config = [
    'mode' => 'normal',

    // 目前仅对多进程模式生效，暂不支持设置为0（不限制）
    'max_conn_per_addr' => 128, // 每个域名最多维持多少并发连接
    'keepalive_timeout' => 86400,  // 连接多长时间不通讯就关闭
    'connect_timeout'   => 86400,  // 连接超时时间
    'timeout'           => 86400,  // 请求发出后等待响应的超时时间
];

return $config;
