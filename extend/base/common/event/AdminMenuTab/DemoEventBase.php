<?php

namespace base\common\event\AdminMenuTab;

use think\facade\Env;
use think\facade\View;

class DemoEventBase
{
    public function handle()
    {
        $content = '';
        $tab_content = '';

        if (Env::get('adminsystem.is_demo', false)) {
            $content = View::layout(false)->fetch('index/ext/admin_menu_tab_demo');
            $tab_content = View::layout(false)->fetch('index/ext/admin_menu_tab_content_demo');
        }

        View::assign('a2','x2');
        View::assign('a3','x3');
        // 事件监听处理
        return [
            'view_content' => $content,
            // AdminMenuTab专有的扩展字段
            'tab_content' => $tab_content,
        ];
    }
}
