<?php

namespace base\common\provider;

use think\facade\Config;

// 应用请求对象类
class RequestBase extends \think\Request
{
    protected $filter = [];

    /**
     * 当前是否Ajax请求
     * @access public
     * @param  bool $ajax true 获取原始ajax请求
     * @return bool
     */
    public function isAjax(bool $ajax = false): bool
    {
        $value  = $this->server('HTTP_X_REQUESTED_WITH');
        $result = $value && 'xmlhttprequest' == strtolower($value) ? true : false;

        if (true === $ajax) {
            return $result;
        }

        $is_ajax = $this->param($this->varAjax) ? true : $result;

        if (!Config::get('app.auto_parse_api')) {
            return $is_ajax;
        }

        if($this->isJson()){
            if ($this->has('get_page_data')) {
                return false;
            }
            return true;
        }

        return $is_ajax;
    }

}
