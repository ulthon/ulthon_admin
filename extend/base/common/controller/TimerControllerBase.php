<?php

namespace base\common\controller;

use app\common\controller\ToolsController;
use think\facade\Cache;

class TimerControllerBase extends ToolsController
{
    protected $frequency = null;

    protected $concurrency = 1;

    protected $concurrencyId = 0;

    public function initialize()
    {
        parent::initialize();

        $concurrency_id = $this->request->param('concurrency_id', 0);
        if ($concurrency_id > $this->concurrency) {
            $this->error('concurrency id error');
        }
        $this->concurrencyId = $concurrency_id;

        $concurrency_count = $this->request->param('concurrency_count', 1);
        if ($concurrency_count > $this->concurrency) {
            $this->error('concurrency count error');
        }

        if (is_int($this->frequency)) {
            $this->protectVisit($this->frequency);
        }
    }

    protected function protectVisit(int $frequency)
    {
        $cache_tag = 'timer_protect';

        $cache_key = 'timer_protect_' . md5($this->request->url());

        $last_exec_time = Cache::get($cache_key, 0);

        if ($last_exec_time >= time() - $frequency) {
            return $this->error('请不要频繁请求');
        }

        Cache::tag($cache_tag)->set($cache_key, time());
    }
}
