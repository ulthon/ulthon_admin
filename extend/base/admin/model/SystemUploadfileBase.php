<?php

namespace base\admin\model;

use app\common\model\TimeModel;

class SystemUploadfileBase extends TimeModel
{
    public const FIELD_LIST_FILE = ['url'];
}
