<?php

namespace base\admin\model;

use app\common\model\TimeModel;

class SystemConfigBase extends TimeModel
{
    protected $deleteTime = false;

    public const FIELD_LIST_FILE = ['value'];
}
